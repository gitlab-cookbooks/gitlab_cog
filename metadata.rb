name 'gitlab_cog'
maintainer 'GitLab BV'
maintainer_email 'john@gitlab.com'
license 'MIT'
description 'Installs/Configures gitlab_cog'
long_description 'Installs/Configures gitlab_cog'
version '0.1.0'
chef_version '>= 12.1' if respond_to?(:chef_version)

# Where people can tell us what we broke
issues_url 'https://gitlab.com/gitlab-cookbooks/gitlab_cog/issues' if respond_to?(:issues_url)

# Where people can find our source
source_url 'https://gitlab.com/gitlab-cookbooks/gitlab_cog' if respond_to?(:source_url)

# Limit the version of chef we respond to
chef_version '>= 12.1' if respond_to?(:chef_version)

depends 'docker', '~> 2.15.13'
